// import { PrismaClient, Prisma } from "@prisma/client";

// const prisma = new PrismaClient();

// const userData: Prisma.UserCreateInput[] = [
//   {
//     name: "Alice",
//     email: "alice@prisma.io",
//     age: 32,
//     country: "Belarus",
//     posts: {
//       create: [
//         {
//           title: "Join the Prisma Slack",
//           published: true,
//         },
//       ],
//     },
//   },
//   {
//     name: "Nilu",
//     email: "nilu@prisma.io",
//     age: 43,
//     country: "China",
//     posts: {
//       create: [
//         {
//           title: "Follow Prisma on Twitter",
//           published: true,
//         },
//       ],
//     },
//   },
//   {
//     name: "Mahmoud",
//     email: "mahmoud@prisma.io",
//     age: 42,
//     country: "Philipines",
//     posts: {
//       create: [
//         {
//           title: "Ask a question about Prisma on GitHub",
//           published: true,
//         },
//         {
//           title: "Prisma on YouTube",
//         },
//       ],
//     },
//   },
// ];

// async function main() {
//   console.log(`Start seeding ...`);
//   for (const u of userData) {
//     const user = await prisma.user.create({
//       data: u,
//     });
//     console.log(`Created user with id: ${user.id}`);
//   }
//   console.log(`Seeding finished.`);
// }

// main()
//   .then(async () => {
//     await prisma.$disconnect();
//   })
//   .catch(async (e) => {
//     console.error(e);
//     await prisma.$disconnect();
//     process.exit(1);
//   });
