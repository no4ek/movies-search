FROM node:16

WORKDIR /usr/src/project/
COPY . .
RUN rm Dockerfile \
    && npm install \
    && npm run generate

EXPOSE 3000
EXPOSE 9229
CMD ["npm", "run", "watch"]