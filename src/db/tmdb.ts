import MovieDB from 'node-themoviedb';

const tmdb = new MovieDB(process.env.TMDB_API_KEY!);

export { tmdb };
