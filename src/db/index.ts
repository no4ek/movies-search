import { NextFunction, Request, Response } from 'express';
import { PrismaClient } from '@prisma/client';

import { elasticsearch } from './elasticsearch';
import { tmdb } from './tmdb';

const prisma = new PrismaClient();

const extendContextWithDb = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  req.context = req.context || {};
  req.context.esdb = elasticsearch;
  req.context.tmdb = tmdb;
  req.context.db = prisma;

  next();
};

export { extendContextWithDb, elasticsearch, tmdb, prisma };
