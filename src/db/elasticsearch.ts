import { Client } from '@elastic/elasticsearch';

const elasticsearch = new Client({
  node: process.env.ES_DB_URL,
});

const initIndex = async (index: string, props: string[]) => {
  const exists = await elasticsearch.indices.exists({
    index,
  });
  if (exists) {
    return;
  }
  await elasticsearch.indices.create({
    index,
    mappings: {
      properties: props.reduce(
        (acc, prop) => ({ ...acc, [prop]: { type: 'text' } }),
        {}
      ),
    },
  });
};

const initEsIndices = () => {
  initIndex('movies', [
    'id',
    'title',
    'originalTitle',
    'overview',
    'actors',
    'director',
  ]);
  initIndex('tvs', ['id', 'name', 'originalName', 'overview', 'actors']);
  initIndex('actors', ['id', 'name', 'originalName']);
};

export { elasticsearch, initEsIndices };
