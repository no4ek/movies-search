import express from 'express';
import morgan from 'morgan';
import * as dotenv from 'dotenv'; // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
dotenv.config();

import { routes } from './routes';
import bodyParser from 'body-parser';
import { extendContextWithDb } from './db';
import { initEsIndices } from './db/elasticsearch';

const app = express();
const port = process.env.PORT;

// TODO use routes
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(extendContextWithDb);

initEsIndices();

app.use(routes);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
