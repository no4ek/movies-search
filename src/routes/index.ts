import express from 'express';
import { storeMoviesFindByQueryController } from './query.controller';

import { errorHandler } from './utils/error.handler';
import { healthController } from './utils/health.controller';

const routes = express.Router();

routes.use('/health', healthController);
routes.post('/movies/query', storeMoviesFindByQueryController);

routes.use(errorHandler);

export { routes };
