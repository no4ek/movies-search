import { Client } from '@elastic/elasticsearch';
import { Prisma } from '@prisma/client';
import { NextFunction, Request, Response } from 'express';
import MovieDB from 'node-themoviedb';
import { prisma } from '../db';
import { z } from 'zod';
import { ObjectId } from 'bson';

const getBody = z.object({
  query: z.string(),
});

type MongoData = {
  movies: Prisma.MovieCreateInput[];
  tvs: Prisma.TvCreateInput[];
  actors: Prisma.ActorCreateInput[];
};

export const storeMoviesFindByQueryController = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { esdb, tmdb } = req.context;

  try {
    const body = getBody.parse(req.body);
    const { query } = body;

    const esItems = await searchInEs(esdb, query);
    if (esItems.length) {
      // should we still find data from moviedb to get updates also??
      // TODO later get from DB by ids and return populated mongo data
      res.json({
        data: esItems.map((hit) => hit._source),
      });
    }

    const tmdbResults = await getMoviesFromTmdb(tmdb, query);
    if (!tmdbResults.length) {
      res.json({
        data: 'Can not find nothing on this query..',
      });
    }

    // check and update if exists in mongo (by externalId) ????ZACEMMMM

    const mongoData: MongoData = {
      movies: [],
      tvs: [],
      actors: [],
    };

    for (const resultItem of tmdbResults) {
      switch (resultItem.media_type) {
        case 'movie': {
          const movieData = await getMovieMongoData(resultItem, tmdb);
          if (movieData) {
            mongoData.movies.push(movieData);
          }
          break;
        }
        case 'tv': {
          break;
        }
        case 'person': {
          break;
        }
        default:
          continue;
      }
    }

    // insert to mongo async
    if (mongoData.movies.length) {
      const a = await prisma.movie.create({ data: mongoData.movies[0] });
      // const a = await prisma.movie.createMany({ data: mongoData.movies });
      console.log('AAA', a);
    }

    // insert to es async
    esdb.bulk({
      refresh: true,
      operations: mongoData.movies.reduce((acc: any, movie) => {
        acc.push(
          { index: { _index: 'movies' } },
          {
            id: movie.id,
            title: movie.title,
            originalTitle: movie.originalTitle,
            overview: movie.overview,
            actors: movie.actors,
            director: movie.director,
          }
        );
        return acc;
      }, []),
    });

    res.json({
      data: mongoData,
    });
  } catch (err: any) {
    next(err);
  }
};

const getMoviesFromTmdb = async (tmdb: MovieDB, query: string) => {
  const tmdbResult = await tmdb.search.multi({
    query: { query: query },
  });
  const { total_pages, results } = tmdbResult?.data;

  // TODO iterate all pagesssss
  return results;
};

const searchInEs = async (esdb: Client, query: string) => {
  const esResult = await esdb.search({
    index: ['movies', 'tvs', 'actors'],
    body: {
      query: {
        multi_match: {
          query: query + '~5',
          fields: [
            'title',
            'originalTitle',
            'overview',
            'name',
            'originalName',
            'director',
          ],
        },
      },
    },
  });

  return esResult?.hits?.hits;
};

const getMovieMongoData = async (
  item: MovieDB.Objects.MovieWithMediaType,
  tmdb: MovieDB
) => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const { title, original_title, overview, id, release_date } = item as any;
  const { data } = await tmdb.movie.getCredits({
    pathParameters: {
      movie_id: id,
    },
  });

  if (!data) {
    return null;
  }

  const { crew, cast } = data;
  const director = crew?.find((c) => c.job === 'Director')?.name;
  // CHECK CAST and UPDATE ACTORS IN MONGO and ES

  const movieData = {
    id: new ObjectId().toString(),
    title: title,
    originalTitle: original_title,
    overview: overview,
    director: director,
    externalId: id,
    releaseDate: release_date,
    // actorIds: TODO cast mongoIds, fetch from tmdb, populate mongoData obj
  } as unknown as Prisma.MovieCreateInput;

  return movieData;
};
