// type MediaType =
//   | 'movie'
//   | 'tv'
//   | 'person'
//   | 'collection'
//   | 'keyword'
//   | 'company'
//   | 'genre'
//   | 'network';

export const MediaTypes = {
  Movie: 'movie',
  Tv: 'tv',
  Person: 'person',
  Collection: 'collection',
  Keyword: 'keyword',
  Company: 'company',
  Genre: 'genre',
  Network: 'network',
};
